package amusementPark;

public class TeaCups extends Attraction {

	private int cryingKids;
	private String teacupColor;
	
	/*
	 * Initiliazes the TeaCup object by calling the super constructor
	 * @param String color of the tea cup
	 * @author Jack Hunter
	 */
	public TeaCups(String name, int maxRiders, int ticketCost, int ticketsSold, double utilityCost, 
			int lengthOfRide, int operationHours, int speed, int height, String color) {
		super(name, maxRiders, ticketCost, ticketsSold, utilityCost, lengthOfRide, operationHours, speed, height);
		
		teacupColor = color;
		cryingKids = ticketsSold / 2;
	}
	
	/*
	 * Gets the color of the tea cup your rode in
	 * @author Jack Hunter
	 */
	public String getColor() {
		return teacupColor;
	}
	
	/*
	 * Gets the number of crying kids in the ride
	 * @author Jack Hunter
	 */
	public int getCryingKids() {
		return cryingKids;
	}

}
