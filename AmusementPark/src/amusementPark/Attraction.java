package amusementPark;

public class Attraction {
	private final String attractionName;
	private final int MAX_RIDERS;
	private double ticketCost;
	private int ticketsLeft;
	private int ticketsSold;
	private double utilityCost;
	private int lengthOfRide;
	private int ridersPerHour;
	private int operationHours;
	private int ridesPerDay;
	private final int speed;
	private final int height;
	
	/* 
	 * Initializes the object and assigns it's instance fields
	 * @param MAX_RIDERS: Total amount of riders one run of the ride can hold
	 * @param ticketCost: The cost of one ticket
	 * @param ticketsSold: The number of tickets already sold
	 * @param utilityCost: Cost of upkeep for one day of operation
	 * @param lengthOfRide: Length of the ride in minutes
	 * @param operationHours: Number of hours the ride is open each day
	 * @param speed: Max speed of the ride in Miles Per Hour
	 * @param height: Max height of the ride
	 * @author Jack Hunter
	 */
	public Attraction(String name, int maxRiders, int ticketCost, int ticketsSold, double utilityCost, 
			int lengthOfRide, int operationHours, int speed, int height) {
		this.attractionName = name;
		this.MAX_RIDERS = maxRiders;
		this.ticketCost = ticketCost;
		this.ticketsSold = ticketsSold;
		this.lengthOfRide = lengthOfRide;
		this.operationHours = operationHours;
		this.speed = speed;
		this.height = height;
		
		ticketsLeft = MAX_RIDERS - ticketsSold;
		ridersPerHour = (int) Math.floor(MAX_RIDERS * (60 / lengthOfRide));
		ridesPerDay = (operationHours * 60) / lengthOfRide;
	}
	
	/*
	 * Gets the cost of a ticket, as declared in the function
	 * @return Double the cost of the ticket
	 * @author Jack Hunter
	 */
	public double getCost() {
		return ticketCost;
	}
	
	/*
	 * Calculates total income for the ride
	 * @return Double the revenue for the ride
	 * @author Jack Hunter
	 */
	private double calculcateRevenue() {
		return ridesPerDay * MAX_RIDERS * ticketCost;
	}
	
	/*
	 * Calculates total profit using the revenue
	 * @return Double the profit for the ride
	 * @author Jack Hunter
	 */
	public double calculateProfit() {
		return this.calculcateRevenue() - utilityCost;
	}
	
	/*
	 * A function to determine how fun a ride is - this is random math
	 * @return Double the amount of fun for the ride
	 * @author Jack Hunter
	 */
	private double calculateFunFactor() {
		return Math.floor((speed * height * lengthOfRide) / ticketCost);
	}
	
	/*
	 * Confirms that there are tickets left to sell, and then updates the instant field
	 * @author Jack Hunter
	 */
	public void buyTicket() {
		if(ticketsLeft <= 0) {
			System.out.println("There are no more tickets for sale!");
		} else {
			ticketsLeft--;
			System.out.printf("You spent $%.2f and purchased a ticket for this attraction! Have fun! \n\n", ticketCost);
		}
	}
	
	/*
	 * Gets the speed of the ride
	 * @return int Speed
	 * @author Jack Hunter
	 */
	public int getSpeed() {
		return speed;
	}
	
	/*
	 * Gets the height of the ride
	 * @return int height
	 * @author Jack Hunter
	 */
	public int getHeight() {
		return height;
	}
	
	/*
	 * Returns the name of the attraction and it's attributes
	 * @return String giving details for the ride
	 * @author Jack Hunter
	 * @Override
	 */
	public String toString() {
		return "Attraction Name: " + attractionName + "    Ticket Cost: $" + ticketCost + "    Ticket Sold: " + ticketsSold + "   Tickets Left: " + ticketsLeft + "    Riders per Hour: " + ridersPerHour + "    Operation Hours: " + operationHours + "    Speed: " + speed + "   Height: " + height + "    Fun Factor: " + this.calculateFunFactor();
	}
	
	
}
