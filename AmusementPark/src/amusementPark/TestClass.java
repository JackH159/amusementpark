package amusementPark;
import java.util.ArrayList;
import java.util.Scanner;
public class TestClass {

	/*
	 * Main class for the project, creates the project and runs the script for the readout
	 * @author Jack Hunter
	 */
	public static void main(String[] args) 
	{
		//Creates objects for the various rides, and an ArrayList to hold them
		ArrayList<Attraction> rides = new ArrayList<Attraction>();

		rides.add(new DropTower("DropTower", 16, 30, 2, 10, 1, 10, 80, 600));
		rides.add(new FerrisWheel("Ferris Wheel", 80, 10, 50, 70, 8, 8, 2, 440, 6));
		rides.add(new TeaCups("Teacups", 8, 15, 5, 10, 2, 3, 4, 2, "Yellow"));
		
		//Initializes the user and starts the interactive part
		double userBalance = 100;
		double minimumCost = 15;
		int cryingKids = 0;
		System.out.println("Welcome to SuperWorld! You have $100 to spend, enjoy your stay! \n");
		System.out.println("Ride Details: ");
		for(Attraction attraction : rides) {
			System.out.println(attraction);
			minimumCost = Math.min(minimumCost, attraction.getCost());
		}
		
		//Calculates balance and allows the user to buy tickets through the console
		Scanner scan = new Scanner(System.in);
		while(userBalance > minimumCost) {
			System.out.printf("\nYou have $%.2f left to spend!", userBalance);
			System.out.println("\nWhich ride would like to try? Type DT for the drop tower, FW for the ferris wheel, or TC for the Teacups");
			String response;
			
			//Allow the user to ride multiple rides
			while(true) {				
				response = scan.nextLine().toLowerCase();
				if(response.equals("dt")) { 
					double rideCost = rides.get(0).getCost();
					if(userBalance + 1 > rideCost) {
						rides.get(0).buyTicket(); 
						userBalance -= rideCost;
					} else {
						System.out.println("You do not have enough money for that ride!");
					}
					break; 
				}
				else if (response.equals("fw")) { 
					double rideCost = rides.get(1).getCost();
					if(userBalance + 1 > rideCost) {
						rides.get(1).buyTicket(); 
						userBalance -= rideCost;
					} else {
						System.out.println("You do not have enough money for that ride!");
					}
					break; 
				}
				else if (response.equals("tc")) { 
					double rideCost = rides.get(2).getCost();
					if(userBalance + 1 > rideCost) {
						rides.get(2).buyTicket(); 
						userBalance -= rideCost;
						cryingKids += ((TeaCups) rides.get(2)).getCryingKids();
					} else {
						System.out.println("You do not have enough money for that ride!");
					}
					break; 
				}
			}
		}
		
		double parkProfit = 0;
		for(Attraction ride : rides) {
			parkProfit += ride.calculateProfit();
		}
		
		//Print out stats about the trip to the amusement park
		System.out.println("You are all out of money! Hope you had fun, come again!\n");
		System.out.println("Some stats from your trip: ");
		System.out.printf("The park made $%.2f of profit today!\n", parkProfit);
		System.out.println("You encountered " + cryingKids + " crying kids and rode in a " + ((TeaCups) rides.get(2)).getColor() + " colored teacup!");
		System.out.println("While riding the death tower, which has a scare factor of " + ((DropTower) rides.get(0)).getScareFactor() + " and a puke rate of " + ((DropTower) rides.get(0)).getPukeRate() + "%, you " + ((DropTower) rides.get(0)).getWillDie());
		((FerrisWheel) rides.get(1)).ifYouLookOverThere();
		System.out.println("On the Ferris Wheel, you had a fall chance of " + ((FerrisWheel) rides.get(1)).getFallChance() + "%");
		scan.close();
	}

}
