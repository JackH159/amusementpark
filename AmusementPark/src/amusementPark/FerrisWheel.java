package amusementPark;
import java.util.Random;

public class FerrisWheel extends Attraction {
	
	private Random random;
	private double fallChance;
	
	/*
	 * Initializes the Ferris Wheel Object, calling the super
	 * @param double fallChance chance of falling
	 * @author Landry
	 */
	public FerrisWheel(String name, int maxRiders, int ticketCost, int ticketsSold, double utilityCost, int lengthOfRide,
			int operationHours, int speed, int height, double fallChance) {
		super(name, maxRiders, ticketCost, ticketsSold, utilityCost, lengthOfRide, operationHours, speed, height);
		
		this.fallChance = fallChance;
		random = new Random();
		/*
		 *  maxRiders = 80
		 *  ticketCost = $10
		 *  ticketsSold = random num from 0 to 80
		 *  utilityCost = $70
		 *  lengthOfRide = 8 mins 
		 *  operationHours = 8 from 12pm - 8pm
		 *  speed = 2 mph
		 *  height = 440 feet
		 *  fallChance = 6%
		 */
	}
	
	/*
	 * Gets the name of a person who died on the ride
	 * @author Landry
	 */
	public void ifYouLookOverThere() {
		// abcdefghijklmnopqrstuvwxyz
		String[] names = {"Abigail","Brock","Cody","David","Effy","Felisha","George","Heisenburg",
						  "Ixchel","Jen","Kliff","Landry","Mark","Nathon","Oprah",
						  "Penny","Quarxy","Roberto","Stephen","Tibolt","Usain",
						  "Vernon","William","Xavier","Zoey"};
		
		System.out.println("Over there on the Ferris Wheel " + names[random.nextInt(names.length)] + 
				" fell " + random.nextInt(441)+ "ft!");
	}
	
	/*
	 * Gets the chance of falling off the ride
	 * @return double Percent chance of falling
	 * @author Landry
	 */
	public double getFallChance() {
		return fallChance;
	}
}
		
