package amusementPark;

import java.util.Random;

/*
 * Name: Andrew T
 * 
 */
public class DropTower extends Attraction 
{
	
	public double MORTALITY_RATE = 25.00;
	public double pukeRate;
	
	/*
	 * initializing objects and assigning instance fieldsS
	 */
	
	public DropTower(String name, int maxRide, int tickCost, int tickSold, double utilCost, int lengthOfRide, int opHours, int spd, int height)
	{
		super(name, maxRide, tickCost, tickSold, utilCost, lengthOfRide, opHours, spd, height);
	}
	
	
	/*
	 * returns the scare factor of my ride which will be shown to user (out of 100%; 100 being the most scary)
	 * @return double Scare factor
	 * @author Andrew Tanquary
	 */
	public double getScareFactor()
	{
		this.pukeRate();
		return pukeRate + (MORTALITY_RATE*2);
	}
	
	/*
	 * calculates the puke rate of my ride based off of speed and height
	 * @return double Puke rate
	 * @author Andrew Tanquary
	 */
	public void pukeRate()
	{
		pukeRate = (super.getSpeed() /10.0) + (super.getHeight() /100.0);
	}
	
	/*
	 * returns a true/false of if the user will die on this ride based off of randomGen mortality rate
	 * @return String Eithier died or did not
	 * @author Andrew Tanquary
	 */
	public String getWillDie()
	{
		Random random = new Random();
		double c = random.nextDouble();
		double rate = MORTALITY_RATE/100;
		if(c>rate)
		{
			return "did not die";
		}
		return "did die";
	}
	
	public double getPukeRate() {
		return pukeRate;
	}
}
